//
//  NotificViewController.swift
//  tableViewTest
//
//  Created by Alina Taimasova on 11.04.18.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import UIKit
import UserNotifications
import Foundation 


class NotificViewController: UIViewController {

    let notificationTimeKey = "notificationTime"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    // loading previously selected date from userDefaults
        
      if let dateValue = UserDefaults().object(forKey: notificationTimeKey) as? Date {
            datePicker.date = dateValue
        }
    }
    
    @IBAction func setButton(_ sender: Any) {
        scheduleNotification()
        showReview()

        // saving selected date to userDefaults
        
        let selectedDate = datePicker.date
        UserDefaults().set(selectedDate, forKey: notificationTimeKey)
    }
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    func scheduleNotification() {
        
        let content = UNMutableNotificationContent() //The notification's content
        
        content.title = NSLocalizedString("notificHeader", comment: "")
        content.sound = UNNotificationSound.default()
        content.body = NSLocalizedString("notificBody", comment: "")
        content.badge = 1
      
        let dateComponent = datePicker.calendar.dateComponents([.hour, .minute], from: datePicker.date) //разбиваем на компоненты
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: true)
        
        let notificationReq = UNNotificationRequest(identifier: "identifier", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(notificationReq, withCompletionHandler: nil)
    }
    
    // Makes Status Bar content white color

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        

   
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
