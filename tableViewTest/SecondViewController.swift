//
//  SecondViewController.swift
//  tableViewTest
//
//  Created by Alina Taimasova on 03.04.18.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var quotes: [String] = []
    var dates: [String] = []
    var photos: [UIImage] = []
   
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dates.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! testCell
        
        cell.dateCell.text = dates[indexPath.row]
        cell.quoteCell.text = quotes[indexPath.row]
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        // loading content for quotes
        
        let quotesObject = UserDefaults.standard.object(forKey: "quotes")
        
        if let tempQuotes = quotesObject {
            
            quotes = tempQuotes as! [String]
            
        }
        // for dates
        
        let datesObject = UserDefaults.standard.object(forKey: "dates")
        
        if let tempDates = datesObject  {
            
            dates = tempDates as! [String]
            }
        tableView.reloadData()
    }

    // add buttons to cells
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let share = UITableViewRowAction(style: .normal, title: NSLocalizedString("sharingButton", comment: ""), handler: { action , indexPath in
            let textToShare = NSLocalizedString("sharingText", comment: "")
           
            let activityController = UIActivityViewController(activityItems: [self.quotes[indexPath.row], textToShare], applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
        })
        
        let delete = UITableViewRowAction(style: .destructive, title: NSLocalizedString("deletingButton", comment: ""), handler: { action , indexPath in
            
            self.dates.remove(at: indexPath.row)
            UserDefaults.standard.set(self.dates, forKey: "dates")
            
            self.quotes.remove(at: indexPath.row)
            UserDefaults.standard.set(self.quotes, forKey: "quotes")
            
            tableView.reloadData()
        })

        return [delete, share]
    }
    
    
    // Makes Status Bar content white color

    override func viewWillAppear(_ animated: Bool) {     
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        
        tableView.reloadData()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


