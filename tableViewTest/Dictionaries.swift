//
//  Dictionaries.swift
//  tableViewTest
//
//  Created by Alina Taimasova on 23.04.2018.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import Foundation
import UIKit

class quotesDict {

var quotesCalendar: Dictionary <String, String> = [
    
    "06-05-2018": "Alohaaaa Puguchinos!",
    
    "07-05-2018": NSLocalizedString("07-05-2018", comment: ""),
    "08-05-2018": NSLocalizedString("08-05-2018", comment: ""),
    "09-05-2018": NSLocalizedString("09-05-2018", comment: ""),
    "10-05-2018": NSLocalizedString("10-05-2018", comment: ""),
    "11-05-2018": NSLocalizedString("11-05-2018", comment: ""),
    "12-05-2018": NSLocalizedString("12-05-2018", comment: ""),
    "13-05-2018": NSLocalizedString("13-05-2018", comment: ""),
    "14-05-2018": NSLocalizedString("14-05-2018", comment: ""),
    "15-05-2018": NSLocalizedString("15-05-2018", comment: ""),
    "16-05-2018": NSLocalizedString("16-05-2018", comment: ""),
    "17-05-2018": NSLocalizedString("17-05-2018", comment: ""),
    "18-05-2018": NSLocalizedString("18-05-2018", comment: ""),
    "19-05-2018": NSLocalizedString("19-05-2018", comment: ""),
    "20-05-2018": NSLocalizedString("20-05-2018", comment: ""),
    "21-05-2018": NSLocalizedString("21-05-2018", comment: ""),
    "22-05-2018": NSLocalizedString("22-05-2018", comment: ""),
    "23-05-2018": NSLocalizedString("23-05-2018", comment: ""),
    "24-05-2018": NSLocalizedString("24-05-2018", comment: ""),
    "25-05-2018": NSLocalizedString("25-05-2018", comment: ""),
    "26-05-2018": NSLocalizedString("26-05-2018", comment: ""),
    "27-05-2018": NSLocalizedString("27-05-2018", comment: ""),
    "28-05-2018": NSLocalizedString("28-05-2018", comment: ""),
    "29-05-2018": NSLocalizedString("29-05-2018", comment: ""),
    "30-05-2018": NSLocalizedString("30-05-2018", comment: ""),
    "31-05-2018": NSLocalizedString("31-05-2018", comment: ""),
    "01-06-2018": NSLocalizedString("01-06-2018", comment: ""),
    "02-06-2018": NSLocalizedString("02-06-2018", comment: ""),
    "03-06-2018": NSLocalizedString("03-07-2018", comment: ""),
    "04-06-2018": NSLocalizedString("04-07-2018", comment: ""),
    "05-06-2018": NSLocalizedString("05-07-2018", comment: ""),
    "06-06-2018": NSLocalizedString("06-07-2018", comment: ""),
    "07-06-2018": NSLocalizedString("07-07-2018", comment: ""),
    "08-06-2018": NSLocalizedString("08-07-2018", comment: ""),
    "09-06-2018": NSLocalizedString("09-07-2018", comment: ""),
    "10-06-2018": NSLocalizedString("10-07-2018", comment: ""),
    "11-06-2018": NSLocalizedString("11-07-2018", comment: ""),
    "12-06-2018": NSLocalizedString("12-07-2018", comment: ""),
    "13-06-2018": NSLocalizedString("13-07-2018", comment: ""),
    "14-06-2018": NSLocalizedString("14-07-2018", comment: ""),
    "15-06-2018": NSLocalizedString("15-07-2018", comment: ""),
    "16-06-2018": NSLocalizedString("16-07-2018", comment: ""),
    "17-06-2018": NSLocalizedString("17-07-2018", comment: ""),
    "18-06-2018": NSLocalizedString("18-07-2018", comment: ""),
    "19-06-2018": NSLocalizedString("19-07-2018", comment: ""),
    "20-06-2018": NSLocalizedString("20-07-2018", comment: ""),
    "21-06-2018": NSLocalizedString("21-07-2018", comment: ""),
    "22-06-2018": NSLocalizedString("22-07-2018", comment: ""),
    "23-06-2018": NSLocalizedString("23-07-2018", comment: ""),
    "24-06-2018": NSLocalizedString("24-07-2018", comment: ""),
    "25-06-2018": NSLocalizedString("25-07-2018", comment: ""),
    "26-06-2018": NSLocalizedString("26-07-2018", comment: ""),
    "27-06-2018": NSLocalizedString("27-07-2018", comment: ""),
    "28-06-2018": NSLocalizedString("28-07-2018", comment: ""),
    "29-06-2018": NSLocalizedString("29-07-2018", comment: ""),
    "30-06-2018": NSLocalizedString("30-07-2018", comment: ""),
    "01-07-2018": NSLocalizedString("01-08-2018", comment: ""),
    "02-07-2018": NSLocalizedString("02-08-2018", comment: ""),
    "03-07-2018": NSLocalizedString("03-08-2018", comment: ""),
    "04-07-2018": NSLocalizedString("04-08-2018", comment: ""),
    "05-07-2018": NSLocalizedString("05-08-2018", comment: ""),
    "06-07-2018": NSLocalizedString("06-08-2018", comment: ""),
    "07-07-2018": NSLocalizedString("07-08-2018", comment: ""),
    "08-07-2018": NSLocalizedString("08-08-2018", comment: ""),
    "09-07-2018": NSLocalizedString("09-08-2018", comment: ""),
    "10-07-2018": NSLocalizedString("10-08-2018", comment: ""),
    "11-07-2018": NSLocalizedString("11-08-2018", comment: ""),
    "12-07-2018": NSLocalizedString("12-08-2018", comment: ""),
    "13-07-2018": NSLocalizedString("13-08-2018", comment: ""),
    "14-07-2018": NSLocalizedString("14-08-2018", comment: ""),
    "15-07-2018": NSLocalizedString("15-08-2018", comment: ""),
    "16-07-2018": NSLocalizedString("16-08-2018", comment: ""),
    "17-07-2018": NSLocalizedString("17-08-2018", comment: ""),
    "18-07-2018": NSLocalizedString("18-08-2018", comment: ""),
    "19-07-2018": NSLocalizedString("19-08-2018", comment: ""),
    "20-07-2018": NSLocalizedString("20-08-2018", comment: ""),
    "21-07-2018": NSLocalizedString("21-08-2018", comment: ""),
    "22-07-2018": NSLocalizedString("22-08-2018", comment: ""),
    "23-07-2018": NSLocalizedString("23-08-2018", comment: ""),
    "24-07-2018": NSLocalizedString("24-08-2018", comment: ""),
    "25-07-2018": NSLocalizedString("25-08-2018", comment: ""),
    "26-07-2018": NSLocalizedString("26-08-2018", comment: ""),
    "27-07-2018": NSLocalizedString("27-08-2018", comment: ""),
    "28-07-2018": NSLocalizedString("28-08-2018", comment: ""),
    "29-07-2018": NSLocalizedString("29-08-2018", comment: ""),
    "30-07-2018": NSLocalizedString("30-08-2018", comment: ""),
    "31-07-2018": NSLocalizedString("31-08-2018", comment: ""),
    
    "01-08-2018": NSLocalizedString("20-04-2018", comment: ""),
    "02-08-2018": NSLocalizedString("21-04-2018", comment: ""),
    "03-08-2018": NSLocalizedString("22-04-2018", comment: ""),
    "04-08-2018": NSLocalizedString("23-04-2018", comment: ""),
    "05-08-2018": NSLocalizedString("24-04-2018", comment: ""),
    "06-08-2018": NSLocalizedString("25-04-2018", comment: ""),
    "07-08-2018": NSLocalizedString("26-04-2018", comment: ""),
    "08-08-2018": NSLocalizedString("27-04-2018", comment: ""),
    "09-08-2018": NSLocalizedString("28-04-2018", comment: ""),
    "10-08-2018": NSLocalizedString("29-04-2018", comment: ""),
    "11-08-2018": NSLocalizedString("30-04-2018", comment: ""),
    "12-08-2018": NSLocalizedString("01-05-2018", comment: ""),
    "13-08-2018": NSLocalizedString("02-05-2018", comment: ""),
    "14-08-2018": NSLocalizedString("03-05-2018", comment: ""),
    "15-08-2018": NSLocalizedString("04-05-2018", comment: ""),
    "16-08-2018": NSLocalizedString("05-05-2018", comment: ""),
    "17-08-2018": NSLocalizedString("06-05-2018", comment: "")
    
]
}
class imagesDict {
    
    let imagesCalendar: Dictionary <String, UIImage> = [
        
        "06-05-2018": #imageLiteral(resourceName: "q10"),
        
        "07-05-2018": #imageLiteral(resourceName: "q11"),
        "08-05-2018": #imageLiteral(resourceName: "q12"),
        "09-05-2018": #imageLiteral(resourceName: "q13"),
        "10-05-2018": #imageLiteral(resourceName: "q14"),
        "11-05-2018": #imageLiteral(resourceName: "q15"),
        "12-05-2018": #imageLiteral(resourceName: "q16"),
        "13-05-2018": #imageLiteral(resourceName: "q17"),
        "14-05-2018": #imageLiteral(resourceName: "q18"),
        "15-05-2018": #imageLiteral(resourceName: "q19"),
        "16-05-2018": #imageLiteral(resourceName: "q20"),
        "17-05-2018": #imageLiteral(resourceName: "q21"),
        "18-05-2018": #imageLiteral(resourceName: "q22"),
        "19-05-2018": #imageLiteral(resourceName: "q23"),
        "20-05-2018": #imageLiteral(resourceName: "q24"),
        "21-05-2018": #imageLiteral(resourceName: "q25"),
        "22-05-2018": #imageLiteral(resourceName: "q26"),
        "23-05-2018": #imageLiteral(resourceName: "q27"),
        "24-05-2018": #imageLiteral(resourceName: "q28"),
        "25-05-2018": #imageLiteral(resourceName: "q29"),
        "26-05-2018": #imageLiteral(resourceName: "q30"),
        "27-05-2018": #imageLiteral(resourceName: "q31"),
        "28-05-2018": #imageLiteral(resourceName: "q32"),
        "29-05-2018": #imageLiteral(resourceName: "q33"),
        "30-05-2018": #imageLiteral(resourceName: "q34"),
        "31-05-2018": #imageLiteral(resourceName: "q35"),
        "01-06-2018": #imageLiteral(resourceName: "q36"),
        "02-06-2018": #imageLiteral(resourceName: "q37"),
        "03-06-2018": #imageLiteral(resourceName: "q38"),
        "04-06-2018": #imageLiteral(resourceName: "q39"),
        "05-06-2018": #imageLiteral(resourceName: "q40"),
        "06-06-2018": #imageLiteral(resourceName: "q41"),
        "07-06-2018": #imageLiteral(resourceName: "q42"),
        "08-06-2018": #imageLiteral(resourceName: "q43"),
        "09-06-2018": #imageLiteral(resourceName: "q44"),
        "10-06-2018": #imageLiteral(resourceName: "q45"),
        "11-06-2018": #imageLiteral(resourceName: "q46"),
        "12-06-2018": #imageLiteral(resourceName: "q47"),
        "13-06-2018": #imageLiteral(resourceName: "q48"),
        "14-06-2018": #imageLiteral(resourceName: "q57"),
        "15-06-2018": #imageLiteral(resourceName: "q58"),
        "16-06-2018": #imageLiteral(resourceName: "q59"),
        "17-06-2018": #imageLiteral(resourceName: "q60"),
        "18-06-2018": #imageLiteral(resourceName: "q61"),
        "19-06-2018": #imageLiteral(resourceName: "q62"),
        "20-06-2018": #imageLiteral(resourceName: "q63"),
        "21-06-2018": #imageLiteral(resourceName: "q64"),
        "22-06-2018": #imageLiteral(resourceName: "q65"),
        "23-06-2018": #imageLiteral(resourceName: "q66"),
        "24-06-2018": #imageLiteral(resourceName: "q67"),
        "25-06-2018": #imageLiteral(resourceName: "q68"),
        "26-06-2018": #imageLiteral(resourceName: "q69"),
        "27-06-2018": #imageLiteral(resourceName: "q70"),
        "28-06-2018": #imageLiteral(resourceName: "q71"),
        "29-06-2018": #imageLiteral(resourceName: "q72"),
        "30-06-2018": #imageLiteral(resourceName: "q73"),
        "01-07-2018": #imageLiteral(resourceName: "q74"),
        "02-07-2018": #imageLiteral(resourceName: "q75"),
        "03-07-2018": #imageLiteral(resourceName: "q76"),
        "04-07-2018": #imageLiteral(resourceName: "q77"),
        "05-07-2018": #imageLiteral(resourceName: "q78"),
        "06-07-2018": #imageLiteral(resourceName: "q79"),
        "07-07-2018": #imageLiteral(resourceName: "q80"),
        "08-07-2018": #imageLiteral(resourceName: "q81"),
        "09-07-2018": #imageLiteral(resourceName: "q82"),
        "10-07-2018": #imageLiteral(resourceName: "q83"),
        "11-07-2018": #imageLiteral(resourceName: "q84"),
        "12-07-2018": #imageLiteral(resourceName: "q85"),
        "13-07-2018": #imageLiteral(resourceName: "q86"),
        "14-07-2018": #imageLiteral(resourceName: "q87"),
        "15-07-2018": #imageLiteral(resourceName: "q88"),
        "16-07-2018": #imageLiteral(resourceName: "q89"),
        "17-07-2018": #imageLiteral(resourceName: "q90"),
        "18-07-2018": #imageLiteral(resourceName: "q91"),
        "19-07-2018": #imageLiteral(resourceName: "q92"),
        "20-07-2018": #imageLiteral(resourceName: "q93"),
        "21-07-2018": #imageLiteral(resourceName: "q94"),
        "22-07-2018": #imageLiteral(resourceName: "q95"),
        "23-07-2018": #imageLiteral(resourceName: "q96"),
        "24-07-2018": #imageLiteral(resourceName: "q97"),
        "25-07-2018": #imageLiteral(resourceName: "q98"),
        "26-07-2018": #imageLiteral(resourceName: "q99"),
        "27-07-2018": #imageLiteral(resourceName: "q100"),
        "28-07-2018": #imageLiteral(resourceName: "q101"),
        "29-07-2018": #imageLiteral(resourceName: "q102"),
        "30-07-2018": #imageLiteral(resourceName: "q103"),
        "31-07-2018": #imageLiteral(resourceName: "q104"),
        
        "01-08-2018": #imageLiteral(resourceName: "q104"),
        "02-08-2018": #imageLiteral(resourceName: "q1"),
        "03-08-2018": #imageLiteral(resourceName: "q2"),
        "04-08-2018": #imageLiteral(resourceName: "q3"),
        "05-08-2018": #imageLiteral(resourceName: "q37"),
        "06-08-2018": #imageLiteral(resourceName: "q55"),
        "07-08-2018": #imageLiteral(resourceName: "q29"),
        "08-08-2018": #imageLiteral(resourceName: "q1"),
        "09-08-2018": #imageLiteral(resourceName: "q2"),
        "10-08-2018": #imageLiteral(resourceName: "q3"),
        "11-08-2018": #imageLiteral(resourceName: "q4"),
        "13-08-2018": #imageLiteral(resourceName: "q5"),
        "14-08-2018": #imageLiteral(resourceName: "q6"),
        "15-08-2018": #imageLiteral(resourceName: "q7"),
        "16-08-2018": #imageLiteral(resourceName: "q8"),
        "17-08-2018": #imageLiteral(resourceName: "q9"),
        "18-08-2018": #imageLiteral(resourceName: "q10")
    ]
}
