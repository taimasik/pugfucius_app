//
//  FirstViewController.swift
//  tableViewTest
//
//  Created by Alina Taimasova on 03.04.18.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import UIKit
import Foundation
import GoogleMobileAds
import StoreKit


class FirstViewController: UIViewController, GADInterstitialDelegate  {
    
        var date = Timer()
        var myDate = Date()
        let calendar = Calendar.current
        var counter: Int = 0
        var selectedRow: String!
        var quotes: [String] = []
        var dates: [String] = []
        var interstitial: GADInterstitial!  // ads

    
        @IBOutlet weak var dataLabel: UILabel!
        @IBOutlet weak var quoteLabel: UILabel!
        @IBOutlet weak var image: UIImageView!
        @IBOutlet weak var rightClickButton: UIButton!
        @IBOutlet weak var leftClickButton: UIButton!
        @IBOutlet weak var addButton: UIButton!
    
    
    @IBOutlet weak var quoteIsAddedMessage: UILabel!
    
    @IBAction func share(_ sender: Any) {
        
        view.isUserInteractionEnabled = true 
        let textToShare = NSLocalizedString("sharingText", comment: "")
        let activityController = UIActivityViewController(activityItems: [image.image!, quoteLabel.text!, textToShare], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
 
   
        @IBAction func rightClick(_ sender: Any) {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM yyyy"
            
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
            let somedateString = dateFormatter.string(from: tomorrow!)
            myDate = tomorrow!
            dataLabel.text = "\(somedateString)"
            
            quoteLabel.text = todaysQuoteString(dateString: dateAsString())
            
            fade()
            
            image.image = todaysImage(dateString: dateAsString())
            
            leftClickButton.isEnabled = true
            
            reloadFavButton()
            
            tomorrowQuoteHidden()
            
            if self.counter == 1 {   // ads load
                
                if interstitial.isReady {
                    interstitial.present(fromRootViewController: self)
                } else {
                    print("Ad wasn't ready")
                }
            }
        }
    
        @IBAction func leftClick(_ sender: Any) {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM yyyy"
            
            let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: myDate)
            myDate = yesterday!
            let somedateString = dateFormatter.string(from: yesterday!)
            dataLabel.text = "\(somedateString)"
            
            quoteLabel.text = todaysQuoteString(dateString: dateAsString())
            
            fade()
            
            image.image = todaysImage(dateString: dateAsString())
            
            rightClickButton.isEnabled = true
            
            reloadFavButton()
            
            finalQuote()
            
            if self.counter == 1 {   // ads load
                
                if interstitial.isReady {
                    interstitial.present(fromRootViewController: self)
                } else {
                    print("Ad wasn't ready")
                }
            }
    }
    
   
    @IBAction func add(_ sender: UIButton) {
        
        addToFavs()
            
        quoteIsAddedMessage.isHidden = true
        quoteIsAddedMessage.alpha = 1
        quoteIsAddedMessage.isHidden = false
        UIView.animate(withDuration: 3, animations: { () -> Void in
            self.quoteIsAddedMessage.alpha = 0
        })
        }

    
    override func viewDidLoad() {
            super.viewDidLoad()
        
            interstitial = createAndLoadInterstitial()  // ads
      
        
            date = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(dateDisplay), userInfo: nil, repeats: false)
            quoteLabel.text = todaysQuoteString(dateString: dateAsString())
            
            fade()

            image.image = todaysImage(dateString: dateAsString())
    }
    
   // for implementing ads
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-2356116573665989/3504011499")
    
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
   
    // for Timer's selector in viewDidLoad
    
        @objc func dateDisplay() {
            
            let date = DateFormatter()
            date.dateFormat = "dd MMMM yyyy"
            
            dataLabel.text = date.string(from: Date())
        }
    
    
    
        // Перевожу дату в Стринг, чтоб по нему индентифицировать цитату
    
        func dateAsString() -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let str = dateFormatter.string(from: myDate as Date)
            return str
        }
        
    // calling Quotes Ductionary from separate file
    
        let quote = quotesDict()
        func todaysQuoteString(dateString: String) -> String {
            return quote.quotesCalendar[dateString]!
        }
     
    // calling Images Ductionary from separate file
    
        let photo = imagesDict()
        func todaysImage(dateString: String) -> UIImage {
            return photo.imagesCalendar[dateString]!
        }
    
    // Makes Tomorrow and Final quotes hidden
    
        func tomorrowQuoteHidden() {
            
            if calendar.isDateInTomorrow(myDate) {
                quoteLabel.text = NSLocalizedString("tomorrowQuote", comment: "")
                rightClickButton.isEnabled = false
                image.image = #imageLiteral(resourceName: "tomm_yest")
                counter = 1
                addButton.isEnabled = false
            }
        }
    
        func finalQuote() {
            if dataLabel.text == "06 May 2018" || dataLabel.text == "06 мая 2018" {
                quoteLabel.text = NSLocalizedString("yesterdayQuote", comment: "")
                leftClickButton.isEnabled = false
                image.image = #imageLiteral(resourceName: "tomm_yest")
                counter = 1
                addButton.isEnabled = false
            }
        }
    
      // Adding to Favorites for quotes
    
        func addToFavs() {
    
        let quotesObject = UserDefaults.standard.object(forKey: "quotes")
        
        if let tempQuotes = quotesObject as? [String] {
            
            quotes = tempQuotes
            
            quotes.append(quoteLabel.text!)
            
        } else {
            
            quotes = [quoteLabel.text!]
        }
            UserDefaults.standard.set(quotes, forKey: "quotes")
        
        
        // Adding to Favorites for dates
        
        let datesObject = UserDefaults.standard.object(forKey: "dates")
        
        if let tempDates = datesObject as? [String] {
            
            dates = tempDates
            
            dates.append(dataLabel.text!)
            
        } else {
            dates = [dataLabel.text!]
        }
        UserDefaults.standard.set(dates, forKey: "dates")
    }
    
    func reloadFavButton() {
        
        self.counter = 0
        self.addButton.isEnabled = true
        
        quoteIsAddedMessage.isHidden = true
        quoteIsAddedMessage.alpha = 1
    }
    
    // animation to fade the photo while appearing
    
    func fade() {
        image.alpha = 0
        UIView.animate(withDuration: 1, animations: {
        self.image.alpha = 1
    })
    }
    
    // Makes Status Bar content white color
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        
        if interstitial.isReady {    //// ads load
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
 
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

    


