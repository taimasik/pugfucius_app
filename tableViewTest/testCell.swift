//
//  testCell.swift
//  tableViewTest
//
//  Created by Alina Taimasova on 03.04.18.
//  Copyright © 2018 Alina Taimasova. All rights reserved.
//

import UIKit

class testCell: UITableViewCell {

    @IBOutlet weak var dateCell: UILabel!
    @IBOutlet weak var quoteCell: UILabel!
    @IBOutlet weak var pugFace: UIImageView!
    
    // Removing grey color when the cell is tapped
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
